﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	void Awake () {
		Debug.Log ("Bonjour");
		if (GameObject.FindGameObjectsWithTag ("MusicManager").Length > 1) {
			Destroy(gameObject);
		}
		DontDestroyOnLoad (transform.gameObject);
		if (PlayerPrefs.GetString ("AudioOn") == "off") {
			AudioListener.pause = true;
		}
	}

	void Start()
	{

	}

	void Update()
	{
		if (PlayerPrefs.GetString ("AudioOn") == "off") {
			AudioListener.pause = true;
		}
	}
}
