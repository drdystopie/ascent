﻿using UnityEngine;
using System.Collections;

public class cameraScript : MonoBehaviour {
	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;
	public Transform target;

//	Camera camera = GetComponent<Camera>();
	
	public Vector2 currentTapCoordinates; 
	
	// Update is called once per frame
	void Update () 
	{

		/*if(Input.GetButtonDown ("Fire1"))
		{
			currentTapCoordinates = camera.ScreenToWorldPoint (Input.mousePosition);
		}*/


		if (target)
		{
			Vector3 point = GetComponent<UnityEngine.Camera>().WorldToViewportPoint(target.position);
			Vector3 delta = target.position - GetComponent<UnityEngine.Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
			Vector3 destination = transform.position + delta;
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
		}
		
	}
}
