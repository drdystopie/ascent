﻿using UnityEngine;
using System.Collections;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
#if UNITY_IPHONE
using UnityEngine.SocialPlatforms;
#endif

public class splashscreen : MonoBehaviour {
	public GameObject logo;
	// Use this for initialization
	IEnumerator Start () {


		yield return new WaitForSeconds(7f);
		Application.LoadLevel ("Menu");

		//Connexion google play
		#if UNITY_ANDROID
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
		
		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
		
		
		Social.localUser.Authenticate((bool success) => {
			if (success) {
				Debug.Log("You've successfully logged in");
			} else {
				Debug.Log("Login failed for some reason");
			}
		});
		#endif



	}
	
	// Update is called once per frame
	void Update () {
		logo.transform.localScale = new Vector2 (logo.transform.localScale.x + 0.0015f, logo.transform.localScale.y + 0.0015f);
	}

	//Enregistre si l'utilisateur ne veux pas etre connecter au google play
	private void socialStatut(bool connected)
	{
		if (connected) {
			PlayerPrefs.SetInt("GooglePlayConnected",1);
		}
		else
		{
			PlayerPrefs.SetInt("GooglePlayConnected",0);
		}
	}
}
