﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour {
	public float speed;
	public new Vector3 positionDepart;
	public new Vector3 positionArriver;
	public bool commenceAGauche;
	// Use this for initialization
	void Start () {
		Debug.Log (positionDepart);
		Debug.Log (positionArriver);
	}
	
	// Update is called once per frame
	void Update () {
		if (commenceAGauche) {
			if (transform.position.x < positionArriver.x) {
				transform.position = positionDepart;
			} else {
				transform.position = new Vector3(transform.position.x-1f*speed,transform.position.y,transform.position.z);
			}

			Debug.Log(transform.position);

		} else {
			if (transform.position.x > positionArriver.x) {
				transform.position = positionDepart;
			} else {
				transform.position = new Vector3(transform.position.x+1f*speed,transform.position.y,transform.position.z);
			}


		}




	}
}
