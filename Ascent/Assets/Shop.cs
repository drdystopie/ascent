﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Shop : MonoBehaviour {
	private GameObject ballonDisplay;
	private Text textPrice;
	private Text textGold;
	public GameObject buyButton;
	public GameObject freeGoldButton;

	private bool buyTime = false;

	private int[] BallonOwn = new int[8]{1,0,0,0,0,0,0,0};
	
	private int[] BallonPrice = new int[8]{0,100,250,500,1000,2000,4000,8000};
	
	// Use this for initialization

	void Start () {
		Advertisement.Initialize ("63679",true);

		ballonDisplay = GameObject.Find ("ballonDisplay");
		textPrice = GameObject.Find ("price").GetComponent<Text> ();
		textGold =  GameObject.Find("txtGoldTop").GetComponent<Text> ();
		buyButton = GameObject.Find("btnBuy");
		if (gameObject.name == "Main Camera") {
			
			
			textGold.text = PlayerPrefs.GetInt("gold")+"";

			PlayerPrefs.SetInt("balloon1Own",1);

			for(int i = 1; i <= BallonOwn.Length;i++ ) {
				//On test si le ballon est deja save
				if (!PlayerPrefs.HasKey ("balloon"+i+"Own")) {
					PlayerPrefs.SetInt("balloon"+i+"Own",BallonOwn[i-1]);
				}
			}
		
		
		//Gestion free gold
			if (PlayerPrefs.HasKey ("freegold")) {

			

				/*DateTime dateClickFreeGold = new DateTime(1970,1,1,0,0,0,0);
				DateTime dateActual = new DateTime();
				dateClickFreeGold = dateClickFreeGold.AddSeconds( PlayerPrefs.GetInt("freegold") );

				Debug.Log(PlayerPrefs.GetInt("freegold"));
				Debug.Log(dateClickFreeGold);
				 
				var diffrenceTime = dateActual.Subtract(dateClickFreeGold).Days; freeGoldButton.GetComponentInChildren<Text>().text = freeGoldButton.GetComponentInChildren<Text>().text+"("+diffrenceTime+")";*/


				int timestampFreeGold = PlayerPrefs.GetInt("freegold");
				int unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;


				Debug.Log (timestampFreeGold);
				DateTime dateFreeGold = new DateTime(1970,1,1,0,0,0,0);
				dateFreeGold = dateFreeGold.AddSeconds( timestampFreeGold);
				Debug.Log(dateFreeGold);

				Debug.Log (unixTimestamp);
				DateTime dateActualPlus2hours = new DateTime(1970,1,1,0,0,0,0);
				dateActualPlus2hours = dateFreeGold.AddSeconds(120*60);
				Debug.Log (dateActualPlus2hours);


				DateTime dateActual = new DateTime(1970,1,1,0,0,0,0);
				dateActual = dateActual.AddSeconds(unixTimestamp);

				if(dateActual < dateActualPlus2hours)
				{
					Debug.Log(dateActualPlus2hours - dateActual);
					TimeSpan remainingTime = dateActualPlus2hours - dateActual;
					freeGoldButton.GetComponentInChildren<Text>().text =  "Free gold in ("+remainingTime.ToString()+")";
					freeGoldButton.GetComponent<Button>().interactable = false;
				}




				//freeGoldButton.GetComponentInChildren<Text>().text = dateClickFreeGold.Date.ToString();
			}
		
		
		}
		

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void selectBalloon(String buttonName)
	{


		if (PlayerPrefs.GetInt("balloon"+buttonName+"Own") == 0) {
			textPrice.text = BallonPrice [int.Parse (buttonName) - 1] + "";
			buyButton.GetComponentInChildren<Text>().text = "Buy";
			buyTime = true;



		} else {
			textPrice.text = "Already"+ "\n" +"bought";
			buyTime = false;
			buyButton.GetComponentInChildren<Text>().text = "Equip";

			if(PlayerPrefs.GetInt("skin_id") == int.Parse(buttonName))
			{
				textPrice.text = "Equiped";
			}
		}

//		ballonDisplay.GetComponent<SpriteRenderer> ().sprite = Resources.Load ("skins/" + buttonName, typeof(Sprite)) as Sprite;

	}
	
	public void BuyBalloon()
	{

		//Achat ou equipement
		if (buyTime) {
			if(PlayerPrefs.GetInt("gold") >= BallonPrice[actualBallonSelector-1])
			{
				PlayerPrefs.SetInt("gold",PlayerPrefs.GetInt("gold")-BallonPrice[actualBallonSelector-1]);
				PlayerPrefs.SetInt("skin_id",actualBallonSelector);

				PlayerPrefs.SetInt("balloon"+actualBallonSelector+"Own",1);

				textGold.text = PlayerPrefs.GetInt("gold")+""	;
				textPrice.text = "already\nbought";
				buyTime = false;
				buyButton.GetComponentInChildren<Text>().text = "Equip";


			}
		} else {
			textPrice.text = "Equiped";
			PlayerPrefs.SetInt("skin_id",actualBallonSelector);
		}

		
		
	}



	public void freegold()
	{
		if (Advertisement.IsReady("rewardedVideo"))
		{
			var options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show("rewardedVideo", options);
		}


		/*bool result = PlayUnityVideoAd ("rewardedVideo");
		//textPrice.text = "result "+result;
		if(result)
			{
				/*PlayerPrefs.SetInt("gold",PlayerPrefs.GetInt("gold") + 100);
				Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
				PlayerPrefs.SetInt("freegold",unixTimestamp);
				textGold.text = PlayerPrefs.GetInt("gold")+"G";
				Application.LoadLevel("Shop");*/
	//		}

	}

	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log("The ad was successfully shown.");
				PlayerPrefs.SetInt("gold",PlayerPrefs.GetInt("gold") + 100);
				Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
				PlayerPrefs.SetInt("freegold",unixTimestamp);
				textGold.text = PlayerPrefs.GetInt("gold")+"";
				Application.LoadLevel("Shop");
			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			break;
		}
	}


	private bool PlayUnityVideoAd(string adName)
	{
		string adString = PlayerPrefs.GetString (adName);
		if (Advertisement.IsReady (adString)) {
			bool resultBool = true;
			Advertisement.Show (adString, new ShowOptions {
				resultCallback = result => {
					switch(result)
					{
						case (ShowResult.Finished):
							PlayerPrefs.SetInt("gold",PlayerPrefs.GetInt("gold") + 100);
							Int32 unixTimestamp = (Int32)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
							PlayerPrefs.SetInt("freegold",unixTimestamp);
							textGold.text = PlayerPrefs.GetInt("gold")+"";
							Application.LoadLevel("Shop");
						break;
						case (ShowResult.Failed):
							resultBool = false;
						break;
						case(ShowResult.Skipped):
							resultBool = false;
						break;
					}

					//textGold.text = result+" - "+resultBool+" - ";

				}
			});
			return resultBool;
		}
		return false;
	}


	private int actualBallonSelector = 1;
	private float timeAnimation;
	private bool cameraIsMoving = false;

	public void NextPreviousBallon(bool next = true)
	{
		if (!cameraIsMoving) {
			buyButton.GetComponent<Button> ().interactable = false;
			
			if (next && actualBallonSelector < 8) {
				timeAnimation = Time.time;
				StartCoroutine(moveto(true));
				
			} else if(!next && actualBallonSelector > 1) {
				timeAnimation = Time.time;
				StartCoroutine(moveto(false));
			}
		}
	}


	private float speedSwipe = 1f;
	IEnumerator moveto(bool next)
	{
		cameraIsMoving = true;
		if (next) {

			while(transform.position.x < actualBallonSelector*6f)
			{
				//Debug.Log(Time.deltaTime*1f);
				//transform.position = Vector3.MoveTowards(transform.position,new Vector3(actualBallonSelector*5,0f,0f),Time.deltaTime*3f);
				transform.localPosition = 
					Vector3.Lerp(
						new Vector3((-1+actualBallonSelector)*6,0f,0f),
						new Vector3(actualBallonSelector*6,0f,0f),
						(Time.time - timeAnimation) / speedSwipe);

				yield return null;

			}
			actualBallonSelector++;
		}
		else{
			Debug.Log(transform.position.x+" > "+(-2+actualBallonSelector)*6f);
			while(transform.position.x > (-2+actualBallonSelector)*6f)
			{
				//transform.position = Vector3.MoveTowards(transform.position,new Vector3((-2+actualBallonSelector)*5f,0f,0f),Time.deltaTime*3f);


				transform.localPosition = 
					Vector3.Lerp(
						new Vector3((-1+actualBallonSelector)*6,0f,0f),
						new Vector3((-2+actualBallonSelector)*6,0f,0f),
						(Time.time - timeAnimation) / speedSwipe);


				yield return null;
				
			}
			actualBallonSelector--;
		}


		selectBalloon (""+actualBallonSelector);
		buyButton.GetComponent<Button> ().interactable = true;
		cameraIsMoving = false;
	}


}
