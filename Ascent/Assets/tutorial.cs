﻿using UnityEngine;
using System.Collections;
using System;


public class tutorial : MonoBehaviour {
	private int etape = 0;

	public GameObject[] imageTuto;
	// Use this for initialization
	void Start () {
		StartCoroutine(FadeOutCR(imageTuto[etape],0.1f,1f,false));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1"))
		{
			Debug.Log("bonjour");
			StartCoroutine(FadeOutCR(imageTuto[etape],1f,0.1f,true));
		}
	}
	
	private IEnumerator FadeOutCR(GameObject gameobject,float beginAlpha,float finishAlpha,bool changescreen)
	{
		float duration = 1f; //0.5 secs
		float currentTime = 0f;
		while(currentTime < duration)
		{
			float alpha = Mathf.Lerp(beginAlpha, finishAlpha, currentTime/duration);
			Color colorGameobject = gameobject.GetComponent<SpriteRenderer>().color;

			gameobject.GetComponent<SpriteRenderer>().color = new Color(colorGameobject.r, colorGameobject.g, colorGameobject.b, alpha);
			currentTime += Time.deltaTime;
			yield return null;


		}

		if (changescreen) {
			//On active un nouveau screen
			imageTuto[etape].SetActive(false);
			etape++;
			imageTuto[etape].SetActive(true);
			StartCoroutine(FadeOutCR(imageTuto[etape],0.1f,1f,false));

		}

		yield break;


	}


}
