﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class mainMenuEffect : MonoBehaviour {

	private bool volStat = false;

	private Vector3 positionBallon;

	public float speedHover;
	public float deplacementHover;

	public GameObject buttonSound;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.HasKey ("skin_id")) {
			Debug.Log("/skins/"+PlayerPrefs.GetInt("skin_id"));
			gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load("skins/"+PlayerPrefs.GetInt("skin_id"), typeof(Sprite)) as Sprite;
		}

		positionBallon = new Vector3 (0f, 0.7f, transform.position.z);


		if (PlayerPrefs.GetString ("AudioOn") == "off") {
			buttonSound.GetComponent<Image> ().sprite = Resources.Load("soundOff", typeof(Sprite)) as Sprite;
		} else {
			buttonSound.GetComponent<Image> ().sprite = Resources.Load("soundOn", typeof(Sprite)) as Sprite;
		}
	}
	
	// Update is called once per frame
	void Update () {

		/*if (transform.position.y < 2.4f && ! volStat) {
			transform.position = new Vector3 (transform.position.x, transform.position.y + 0.3f * Time.deltaTime, transform.position.z);
		} else if (transform.position.y > 2.4f && ! volStat) {
			positionBallon = transform.position;
			volStat = true;
		}

		if (volStat) {
			Vector3 v = positionBallon;
			v.y += 0.05f * Mathf.Sin (Time.time * 1f);
			transform.position = v;
		}*/


		if (transform.position.y < 0.7f && ! volStat) {
			transform.position = Vector3.MoveTowards (transform.position, new Vector3 (transform.position.x, 0.7f, transform.position.z), 0.3f * Time.deltaTime);
		}

		if (transform.position.y == 0.7f) {
			volStat = true;
		}

		if (volStat) {
			Vector3 v = transform.position;
			v.y += deplacementHover * Mathf.Sin (Time.time * speedHover);
			transform.position = v;
		}

	}
}
