﻿using UnityEngine;
using System.Collections;

public class tutorial2 : MonoBehaviour {
	public GameObject[] images;
	public int stepTuto = 0;
	private float timeAnimation;
	// Use this for initialization
	void Start () {
		Camera.main.aspect = 9f / 16f;
		PlayerPrefs.SetInt ("firstGame", 1);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1") && !cameraIsMoving)
		{
			Debug.Log("click");
			if(stepTuto == 3)
			{
				Application.LoadLevel("Main4");
			}
			else
			{
				timeAnimation = Time.time;
				StartCoroutine(moveto(5.65f));
			}


		}
	}



	/*private IEnumerator MoveObject(GameObject target, float speed)
	{
		isMoving = true;
		float step = speed * Time.deltaTime;
		Debug.Log ("dddddddddddd"+target);
		while (true)
		{
			//timeSinceStarted += Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position,new Vector3(target.gameObject.transform.position.x,target.gameObject.transform.position.y,transform.position.z),step);
			
			// If the object has arrived, stop the coroutine
			
			if (transform.position.x == target.gameObject.transform.position.x && transform.position.y == target.gameObject.transform.position.y)
			{
				timeAnimation = Time.time;
				Debug.Log("arriver");
				isMoving = false;
				stepTuto++;
				yield break;
			}
			
			// Otherwise, continue next frame
			yield return null;
		}
		
	}*/

	private bool cameraIsMoving = false;
	private float speedSwipe = 1.7f;
	IEnumerator moveto(float espacement)
	{
		cameraIsMoving = true;
		Debug.Log (transform.position.x + " < " + stepTuto * espacement);

		while(transform.position.x < stepTuto*espacement)
			{
				Debug.Log(stepTuto*espacement);
				
				transform.localPosition = 
					Vector3.Lerp(
					new Vector3((-1+stepTuto)*espacement,0f,transform.position.z),
					new Vector3(stepTuto*espacement,0f,transform.position.z),
					(Time.time - timeAnimation) / speedSwipe);
				
				yield return null;
				
			}
			stepTuto++;
		
		cameraIsMoving = false;
	}
}
