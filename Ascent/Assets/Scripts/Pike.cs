﻿using UnityEngine;
using System.Collections;

public class Pike : MonoBehaviour {
	private GameObject player;
	private float speedPike;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("player");
		Main main = player.GetComponent<Main> ();
		speedPike = Random.Range(2f,5f);

		if (speedPike > 4f) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load ("fireballRED", typeof(Sprite)) as Sprite;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (transform.position.y < -11f) {
			Destroy(gameObject);

		} 
		//On ne déplace les météores que si le gamestate est inGame
		else if(Main.getActualGameState() == Main.gameState.inGame){
			transform.position = new Vector3 (transform.position.x, transform.position.y - speedPike * Time.deltaTime, 0);
		
		}


	}


}
