﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using UnityEngine.Advertisements;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif
#if UNITY_IPHONE
using UnityEngine.SocialPlatforms;
#endif

public class Menu : MonoBehaviour {


	public GameObject loadtime;

	public GameObject buttonSound;


	// Use this for initialization
	void Start () {

		#if UNITY_IPHONE
		Social.localUser.Authenticate ( success => {
			if (success) {
				Debug.Log("==iOS GC authenticate OK"); 
			} 
			else
			{
				Debug.Log("==iOS GC authenticate Failed"); 
			} 
			//Le splashScreen apparait pendant 3 secondes
			
			
		} );
		#endif

		Advertisement.Initialize ("63679",true);

		//On ne connecte le joeuur au google play s'il a dit oui dans le splashscreen
		if (PlayerPrefs.GetInt("GooglePlayConnected")==1) {
			//Social.Active = new UnityEngine.SocialPlatforms.GPGSocial();
			//Social.localUser.Authenticate((bool obj) => Debug.Log("good"));
		}
	}


	public void GoToGame()
	{
		if (PlayerPrefs.GetInt ("firstGame") == 0) {
			Application.LoadLevel ("tutorial");
		} else {
			Application.LoadLevel ("Main4");
		}

	}



	public void QuitGame()
	{
		Application.Quit ();
	}

	public void rebootLevel()
	{
		if (PlayerPrefs.HasKey ("partyNumber")) {
			if(PlayerPrefs.GetInt("partyNumber") == 5)
			{
				PlayerPrefs.SetInt("partyNumber",1);
				if (Advertisement.IsReady ()) {
					Advertisement.Show("incentive", new ShowOptions {
						resultCallback = result => {
							Application.LoadLevel ("Main4");
						}
					});
				}

			}
			else
			{
				PlayerPrefs.SetInt("partyNumber",PlayerPrefs.GetInt("partyNumber") + 1);
				Application.LoadLevel ("Main4");
			}
			
		} else {
			PlayerPrefs.SetInt("partyNumber",1);
			Debug.Log("loadlevel");
			Application.LoadLevel ("Main4");
		}


	}

	public void mainMenu()
	{
		Application.LoadLevel ("Menu");
	}

	#if UNITY_IPHONE
	ILeaderboard m_Leaderboard {
		get;
		set;
	}
	#endif
	
	public void showLeaderboard()
	{
		#if UNITY_ANDROID
			PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkIrN6tjoMcEAIQAQ");
		#endif
		#if UNITY_IPHONE
		m_Leaderboard = Social.CreateLeaderboard();
		m_Leaderboard.id = "com.pixelcoffee.ascent.leaderboard";
		m_Leaderboard.LoadScores( success => { if (success) { Debug.Log("load score ok"); } else { Debug.Log("load sxore failed"); } } );
		Social.ShowLeaderboardUI();
		#endif
	}
	
	public void goToShop()
	{
		
		Application.LoadLevel ("shop");
	}
	

	void OnAuthCB(bool result)
	{
		Debug.Log("GPGUI: Got Login Response: " + result);
	}

	public void OnSubmitScore(bool result)
	{
		Debug.Log("GPGUI: OnSubmitScore: " + result);
	}

	public void goToPlay()
	{
		
		Application.OpenURL ("https://itunes.apple.com/app/id1067944498");
	}

	public void MusicOnOff()
	{
		if (AudioListener.pause) {
			AudioListener.pause = false;
			PlayerPrefs.SetString("AudioOn","on");
			buttonSound.GetComponent<Image> ().sprite = Resources.Load("soundOn", typeof(Sprite)) as Sprite;
		} else {
			AudioListener.pause = true;
			PlayerPrefs.SetString("AudioOn","off");
			buttonSound.GetComponent<Image> ().sprite = Resources.Load("soundOff", typeof(Sprite)) as Sprite;
		}

	}



}
