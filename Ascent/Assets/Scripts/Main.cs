using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.SocialPlatforms;
using UnityEngine.Advertisements;

public class Main : MonoBehaviour {
private List<platform> listPlatform;

	private GameObject gameObjectMainsScore;
	private Text gameObjectMainsScoreText;

	private GameObject gameObjectCombo;
	private Text gameObjectMainsComboText;

	private int position = 5;

	private float speed = 3f;
	
	private float score = 0;
	private int nbplateforme = 0;

	private GameObject top_hover;
	private GameObject top_hover_balloon;

	//Sprite des obstacles
	private Object pikeSprite;
	
	//Proriétés statiques
	public static bool gameOver = false;

	private bool firstDeath = true;

	private GameObject pauseButton;

	//GameObject reference publique
	public GameObject overlayGameOver;
	public GameObject overlaySecondChance;
	public GameObject overlayPauseButton;
	public GameObject explosionAnimation;
	public GameObject new_records;
	public GameObject skipSecondChanceText;
	public GameObject background;


	//Spwan ennemies
	private float speedOfSpawn = 0.5f;

	private Text allGold;

	public enum gameState {inGame,pauseGame,gameOver}
	public static gameState actualGameState;

	void Start () {
		actualGameState = gameState.inGame;
		//On ne connecte le joeuur au google play s'il a dit oui dans le splashscreen
		if (PlayerPrefs.GetInt("GooglePlayConnected")==1) {
			//Social.Active = new UnityEngine.SocialPlatforms.GPGSocial();
			//Social.localUser.Authenticate((bool obj) => Debug.Log("good"));
		}



		Advertisement.Initialize ("63679",true);

		//Gameobject Score & Combo
		gameObjectMainsScore = GameObject.Find ("score");
		gameObjectMainsScoreText = gameObjectMainsScore.GetComponent<Text> ();
		gameObjectCombo = GameObject.Find ("combo");
		gameObjectMainsComboText = gameObjectCombo.GetComponent<Text> ();

		//Gameobject barre meilleur score
		top_hover = GameObject.Find ("top_hover");
		top_hover_balloon = GameObject.Find ("top_hover_balloon");

		//Création de la liste des plateformes
		listPlatform = new List<platform> ();
		for (int i =1; i <=9; i++) {
			//Plateforme principale, name, plateforme hover
			listPlatform.Add(new platform(GameObject.Find ("platform_" + i),"platform_" + i,GameObject.Find ("platform_" + i + "_hover"),GameObject.Find ("platform_" + i).transform.position,GameObject.Find ("platform_" + i + "_hover").transform.position));
		}

		//Sprite
		pikeSprite = Resources.Load ("line", typeof(GameObject));

		//Création des météores
		StartCoroutine ("SpawnEnnemies");

		//On cherche si le joueur a un skin selectionner
		if (PlayerPrefs.HasKey ("skin_id")) {
			Debug.Log ("/skins/" + PlayerPrefs.GetInt ("skin_id"));
			gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load ("skins/" + PlayerPrefs.GetInt ("skin_id"), typeof(Sprite)) as Sprite;
			top_hover_balloon.GetComponent<SpriteRenderer> ().sprite = Resources.Load ("skins/" + PlayerPrefs.GetInt ("skin_id"), typeof(Sprite)) as Sprite;
		} else {
			gameObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load ("skins/" + 1, typeof(Sprite)) as Sprite;
			top_hover_balloon.GetComponent<SpriteRenderer> ().sprite = Resources.Load ("skins/" + 1, typeof(Sprite)) as Sprite;
		}

		pauseButton = GameObject.Find ("pauseButton");

	}
	
	void Update () {
		switch (actualGameState) {
		case gameState.inGame:
			{
				//Coordonnees du tap
				if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) {
					Touch touch = Input.GetTouch (0);
					if (touch.position.x < Screen.width / 2) {	
						leftMovement ();
					} else if (touch.position.x > Screen.width / 2) {
						rightMovement ();
					}
				}
					
				if (Input.GetKeyDown (KeyCode.LeftArrow)) {
					leftMovement ();
				}
					
				if (Input.GetKeyDown (KeyCode.RightArrow)) {
					rightMovement ();
				}
					
				nbplateforme = 0;
				for (int i = 1; i <=9; i++) {
						
					if (i == position) {
						listPlatform [i - 1].getGameObjectOver ().transform.position = Vector3.MoveTowards (listPlatform [i - 1].getGameObjectOver ().transform.position, new Vector3 (listPlatform [i - 1].getGameObject ().transform.position.x, listPlatform [i - 1].getGameObject ().transform.position.y, listPlatform [i - 1].getGameObjectOver ().transform.position.z), speed * Time.deltaTime);
					} else {
							
						listPlatform [i - 1].getGameObjectOver ().transform.position = Vector3.MoveTowards (listPlatform [i - 1].getGameObjectOver ().transform.position, listPlatform [i - 1].getPositionBaseHover (), speed * Time.deltaTime / 3f);
					}
						
					if (listPlatform [i - 1].getGameObjectOver ().transform.position.y > listPlatform [i - 1].getPositionBaseHover ().y) {
						nbplateforme++;
					}
						
				}
					
				//Calcul du score
			float scoreToAdd = Mathf.Pow (nbplateforme, 2) / 100;
			score += scoreToAdd;
					
				gameObjectMainsScoreText.text = "<size=190>" + (int)score + "</size>";
				gameObjectMainsComboText.text = "<size=50>x" + (int)nbplateforme + "</size>";
					
				
				
				//Gestion de la barre meilleur score
				if (PlayerPrefs.HasKey ("bestscore") && !gameOver) {
					if (score > 1 && score < PlayerPrefs.GetInt ("bestscore")) {
						float totalDeplacement = -2.2465f;
						float deplacementParPtsDeScore = totalDeplacement / PlayerPrefs.GetInt ("bestscore");
						
						top_hover.transform.position = new Vector3 (totalDeplacement - deplacementParPtsDeScore * score, top_hover.transform.position.y, top_hover.transform.position.z);
					}
					
					if (PlayerPrefs.GetInt ("bestscore") < score & !gameOver) {
						new_records.SetActive (true);
					}
				}

				
			//Distance a parcourir 12f
			//2000 = 12f
			//float distanceToAddBG = score / 2000 * 100;


			if(score <= 1000)
			{
				background.transform.position = new Vector2(background.transform.position.x, (-1*score*12/1000)-1);
			}

		
				break;
			}
		}
	}



	//Action quand le ballon prend une météore
	void OnTriggerEnter2D(Collider2D coll) {


		if (coll.gameObject.name == "line"||coll.gameObject.name == "line(Clone)")  {
			GetComponent<BoxCollider2D> ().enabled = false;

			actualGameState = gameState.gameOver;

			//On fais disparaite le personnage principal
			GetComponent<Renderer>().enabled = false;

			//explosion
			GameObject instance;
			instance = Instantiate(explosionAnimation,new Vector3(transform.position.x,transform.position.y,-1f), Quaternion.identity) as GameObject;;

			if(firstDeath)
			{
				Debug.Log ("firstdeath : "+coll);
				actualGameState = gameState.pauseGame;
				pauseButton.SetActive(false);
				StartCoroutine(displaySecondChance());
			}
			else
			{
				Debug.Log ("seconddeath : "+coll);
				Debug.Log (coll);
				pauseButton.SetActive(false);
				actualGameState = gameState.pauseGame;
				StartCoroutine(displayGameOver());
			}

		}

	}

	//Coroutine


	IEnumerator displaySecondChance()
	{	
		Debug.Log ("waitSecondChance");

		firstDeath = false;

		yield return new WaitForSeconds (1f);
		//Mode fantome
		GetComponent<SpriteRenderer> ().color = new Color (GetComponent<SpriteRenderer> ().color.r, GetComponent<SpriteRenderer> ().color.g, GetComponent<SpriteRenderer> ().color.b,0.180F);

		overlaySecondChance.SetActive (true);

		allGold = GameObject.Find("textAllGold").GetComponent<Text>();
		allGold.text = PlayerPrefs.GetInt("gold")+"";
	}

	ILeaderboard m_Leaderboard {
		get;
		set;
	}



	IEnumerator displayGameOver() {
		Debug.Log ("waitGameOver");



		//Affichage overlay game over
		overlayGameOver.SetActive(true);
		gameObjectMainsScore.SetActive (false);

		//Enregistrement du score s'il à battu sont record personel
		if(PlayerPrefs.GetInt("bestscore") < score)
		{
			PlayerPrefs.SetInt("bestscore", (int)score);
			
			#if UNITY_ANDROID && !UNITY_EDITOR
			Social.ReportScore((long)score, "CgkIrN6tjoMcEAIQAQ", OnSubmitScore);
			#endif

			#if UNITY_IPHONE
			m_Leaderboard = Social.CreateLeaderboard();
			m_Leaderboard.id = "com.pixelcoffee.ascent.leaderboard";

			Social.ReportScore((long)score, "com.pixelcoffee.ascent.leaderboard", OnSubmitScore);
			#endif
	

		}

		Text textBestScore = GameObject.Find("overlayBestScoreText").GetComponent<Text>();
		textBestScore.text = PlayerPrefs.GetInt("bestscore").ToString();
		
		Text textScore = GameObject.Find("overlayScoreText").GetComponent<Text>();
		textScore.text = (int)score+"";


		Text textGold = GameObject.Find("textGold").GetComponent<Text>();
		int goldGain = (int)score / 10;
		textGold.text = ""+(int)goldGain;
		PlayerPrefs.SetInt ("gold", PlayerPrefs.GetInt("gold")+goldGain);

		yield return new WaitForSeconds (0.5f);
	}

	public void continueGame(bool withGold = true)
	{
		if (withGold) {
			if(PlayerPrefs.GetInt("gold") >= 500)
			{
				overlaySecondChance.SetActive (false);
				GetComponent<Renderer>().enabled = true;
				PlayerPrefs.SetInt ("gold", PlayerPrefs.GetInt("gold")-500);
				allGold.text = PlayerPrefs.GetInt("gold")+"G";
				StartCoroutine (returnToNormal());
			}
		}
		//On regarde une vidéo
		else {
			if (Advertisement.IsReady("rewardedVideo"))
			{
				var options = new ShowOptions { resultCallback = HandleShowResult };
				Advertisement.Show("rewardedVideo", options);
			}
		}
	}

	private void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log("The ad was successfully shown.");
			gameOver = false;
			overlaySecondChance.SetActive (false);
			GetComponent<Renderer>().enabled = true;
			StartCoroutine (returnToNormal());
			break;
		case ShowResult.Skipped:
			Debug.Log("The ad was skipped before reaching the end.");
			break;
		case ShowResult.Failed:
			Debug.LogError("The ad failed to be shown.");
			break;
		}
	}



	public void skipSecondChance()
	{
		overlaySecondChance.SetActive (false);
		StartCoroutine(displayGameOver());
	}

	IEnumerator returnToNormal() {
		actualGameState = gameState.inGame;
		pauseButton.SetActive(true);
		yield return new WaitForSeconds (3f);



		GetComponent<SpriteRenderer> ().color = new Color (GetComponent<SpriteRenderer> ().color.r, GetComponent<SpriteRenderer> ().color.g, GetComponent<SpriteRenderer> ().color.b,1F);
		GetComponent<BoxCollider2D>().enabled = true;

	}

	//Coroutine


	//Coroutine Spawn ennemies
	private IEnumerator SpawnEnnemies()
	{
		while (true) {
			if (actualGameState == gameState.inGame) {
				int platformNumber = (int)Random.Range (1f, 10f);
				GameObject instance;
				instance = Instantiate (pikeSprite, new Vector2 (listPlatform [platformNumber - 1].getGameObject ().transform.position.x, listPlatform [platformNumber - 1].getGameObject ().transform.position.y + 8f), Quaternion.identity) as GameObject;
				if (speedOfSpawn > 0.17f) {
					speedOfSpawn -= 0.003f;
					//Debug.Log(speedOfSpawn);
				}
				else
				{
					Debug.Log("Max speed");
				}
			}
			yield return new WaitForSeconds (speedOfSpawn);
		}
	}

	void leftMovement()
	{
		if (position > 1) {
			position--;
			transform.position = new Vector3 (transform.position.x - 0.25f, transform.position.y, transform.position.z);
		} else {
			transform.position = new Vector3 (1f, transform.position.y, transform.position.z);
			position = 9;
		}
	}
	
	void rightMovement()
	{
		if (position < 9) {
			position++;
			transform.position = new Vector3 (transform.position.x + 0.25f , transform.position.y , transform.position.z);
		} else {
			transform.position = new Vector3 (-1f, transform.position.y, transform.position.z);
			position = 1;
		}
	}
	
	
	public bool getGameover()
	{
		return gameOver;
	}
	
	public void OnSubmitScore(bool result)
	{
		Debug.Log("GPGUI: OnSubmitScore: " + result);
	}
	
	void OnAuthCB(bool result)
	{
		Debug.Log("GPGUI: Got Login Response: " + result);
	}

	private bool PlayUnityVideoAd(string adName)
	{
		string adString = PlayerPrefs.GetString ("UnityAds" + adName);
		if (Advertisement.IsReady (adString)) {
			bool resultBool = true;
			Advertisement.Show (adString, new ShowOptions {
				resultCallback = result => {
					switch(result)
					{
					case (ShowResult.Finished):
						gameOver = false;
						overlaySecondChance.SetActive (false);
						GetComponent<Renderer>().enabled = true;
						StartCoroutine (returnToNormal());
						break;
					case (ShowResult.Failed):
						resultBool = false;
						break;
					case(ShowResult.Skipped):
						resultBool = false;
						break;
					}

					
				}
			});
			return resultBool;
		}
		return false;
	}

	public static gameState getActualGameState()
	{
		return actualGameState;
	}

	public void pauseGame()
	{
		if (actualGameState == gameState.inGame) {
			pauseButton.SetActive(false);
			//pauseButton.GetComponent<Image> ().sprite = Resources.Load("playButton", typeof(Sprite)) as Sprite;
			overlayPauseButton.SetActive(true);
			actualGameState = gameState.pauseGame;
		} else {
			pauseButton.SetActive(true);
			pauseButton.GetComponent<Image> ().sprite = Resources.Load("pauseButton", typeof(Sprite)) as Sprite;
			overlayPauseButton.SetActive(false);
			actualGameState = gameState.inGame;
		}

	}
	
}
