﻿using UnityEngine;
using System.Collections;

public class facebook : MonoBehaviour {
	
	private string AppID = "537940779690073";
	private string Link = "https://play.google.com/store/apps/details?id=com.pixelcoffee.ascent";
	// The picture's URL and the picture must be at least 200px by 200px.
	private string Picture = "https://lh3.googleusercontent.com/DTlyr_Ex5jyTQWcemGny706l2qejXoS_YKKlWj4ER_XQsOzjpFI1UZXqWXF9JE5Y5DQ=w300";
	
	// The name of your app or game.
	private string Name = "Ascent";



	// The caption of your game or app.
	private string Caption; 
	
	// The description of your game or app.
	private string Description = "";
	
	public void FacebookShare(){
		Application.OpenURL ("https://www.facebook.com/dialog/feed?_path=feed&app_id=537940779690073&redirect_uri=https://facebook.com&sdk=joey&display=popup&caption=&description="+SpaceHere(Description)+"&message&name=Ascent&picture=https://lh3.googleusercontent.com/DTlyr_Ex5jyTQWcemGny706l2qejXoS_YKKlWj4ER_XQsOzjpFI1UZXqWXF9JE5Y5DQ=w300&link="+Link+"&from_login=1#_=_");
	}
	string SpaceHere (string val) {
		return val.Replace(" ", "%20"); // %20 is only used for space
	}

	void Start () {
		Caption  = "My best Score in Ascent is "+PlayerPrefs.GetInt ("bestscore")+" Can u beat it?";
	}
}