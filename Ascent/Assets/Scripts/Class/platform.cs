﻿using UnityEngine;
using System.Collections;

public class platform {

	//Pouvoir de 0 à 100
	private string name;
	private int power = 0;
	private GameObject gameObject;
	private GameObject gameObjectOver;
	private Vector3 positionBase;
	private Vector3 positionBaseHover;


	public platform(GameObject gameObject,string name,GameObject gameObjectOver,Vector3 positionBase,Vector3 positionBaseHover)
	{
		this.gameObject = gameObject;
		this.name = name;
		this.gameObjectOver = gameObjectOver;
		this.positionBase = positionBase;
		this.positionBaseHover = positionBaseHover;
	}


	public GameObject getGameObject()
	{
		return this.gameObject;
	}

	public string  getName()
	{
		return this.name;
	}

	public GameObject getGameObjectOver()
	{
		return this.gameObjectOver;
	}


	public Vector3 getPositionBase()
	{
		return this.positionBase;
	}

	public Vector3 getPositionBaseHover()
	{
		return this.positionBaseHover;
	}


}
